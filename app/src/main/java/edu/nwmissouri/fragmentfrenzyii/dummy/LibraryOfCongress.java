package edu.nwmissouri.fragmentfrenzyii.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class LibraryOfCongress {

    /**
     * An array of sample (dummy) items.
     */
    public static List<Book> BOOKS = new ArrayList<Book>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, Book> ITEM_MAP = new HashMap<String, Book>();

    static {
        // Add 3 sample items.
        addItem(new Book("1", "Lord of the Rings"));
        addItem(new Book("2", "Sherlock Holmes"));
        addItem(new Book("3", "The Life of Pi"));
        addItem(new Book("3", "The Life of e"));
    }

    private static void addItem(Book item) {
        BOOKS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class Book {
        public String id;
        public String content;

        public Book(String id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
